import os
import urllib
import datetime
import time
import uuid
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.ext import db

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

def resource_key(current_date):
    return ndb.Key('Current_Date', current_date)

class Resource(ndb.Model):
    ResourceID = ndb.StringProperty(indexed=True)
    ResourceName = ndb.StringProperty(indexed=True)
    StartTime = ndb.StringProperty(indexed=False)
    EndTime = ndb.StringProperty(indexed=False)
    OwnerName = ndb.StringProperty(indexed=True)
    LastReservationTime = ndb.DateTimeProperty(indexed=True)
    Counter = ndb.IntegerProperty(indexed=False)
    Tags = ndb.StringProperty(repeated=True)

def booking_key(user_ID):
    return ndb.Key('User_ID', user_ID)

class Booking(ndb.Model):
    BookingID = ndb.StringProperty(indexed=True)
    UserID = ndb.StringProperty(indexed=True)
    StartTime = ndb.StringProperty(indexed=False)
    EndTime = ndb.StringProperty(indexed=False)
    Duration = ndb.StringProperty(indexed=False)
    ResourceID = ndb.StringProperty(indexed=True)
    ResourceName = ndb.StringProperty(indexed=True)

class MainPage(webapp2.RequestHandler):

    def get(self):
        # Checks for active Google account session
        user = users.get_current_user()

        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'

            MyReservationList = list(Booking.gql("WHERE UserID = :1" , user.nickname()))
            MyResourceList = list(Resource.gql("WHERE OwnerName = :1" , user.nickname()))
            SystemResourceList = list(Resource.gql(""))
            MyActiveReservationList = []

            MyReservationLen = len(MyReservationList)
            MyResourceLen = len(MyResourceList)
            SystemResourceLen = len(SystemResourceList)

            for passnum in range(len(MyResourceList)-1,0,-1):
                for i in range(passnum):
                    if MyResourceList[i].LastReservationTime < MyResourceList[i+1].LastReservationTime:
                        temp = MyResourceList[i]
                        MyResourceList[i] = MyResourceList[i+1]
                        MyResourceList[i+1] = temp

            for passnum in range(len(SystemResourceList)-1,0,-1):
                for i in range(passnum):
                    if SystemResourceList[i].LastReservationTime < SystemResourceList[i+1].LastReservationTime:
                        temp = SystemResourceList[i]
                        SystemResourceList[i] = SystemResourceList[i+1]
                        SystemResourceList[i+1] = temp

            for passnum in range(len(MyReservationList)-1,0,-1):
                for i in range(passnum):
                    if MyReservationList[i].StartTime > MyReservationList[i+1].StartTime:
                        temp = MyReservationList[i]
                        MyReservationList[i] = MyReservationList[i+1]
                        MyReservationList[i+1] = temp

            for i in range(len(MyReservationList)):
                if MyReservationList[i].EndTime > getCurrentTime():
                    MyActiveReservationList.append(MyReservationList[i])

            MyActiveReservationLen = len(MyActiveReservationList)

            template_values = { 'MyResourceList': MyResourceList,
                            'SystemResourceList': SystemResourceList,
                            'MyReservationList': MyActiveReservationList,
                            'MyReservationLen': MyActiveReservationLen,
                            'MyResourceLen': MyResourceLen,
                            'SystemResourceLen': SystemResourceLen,
                            'url': url,
                            'url_linktext': url_linktext,
                            'source': "1"
                }

            template = JINJA_ENVIRONMENT.get_template('HomePageHTML.html')
            self.response.write(template.render(template_values))

        else:
            self.redirect(users.create_login_url(self.request.uri))

class SearchResource(webapp2.RequestHandler):
    def post(self):
        ResourceName = self.request.get("ResourceName")
        ResourceList = list(Resource.gql("WHERE ResourceName = :1" , ResourceName))
        PartialResourceList = []
        AllResourceList = list(Resource.gql(""))

        for resource in AllResourceList:
            if ResourceName in resource.ResourceName:
                PartialResourceList.append(resource)

        template_values = { 'ResourceList': ResourceList,
                            'ResourceListLen': len(ResourceList),
                            'PartialResourceList': PartialResourceList,
                            'PartialResourceLen': len(PartialResourceList)
        }

        template = JINJA_ENVIRONMENT.get_template('ResourcesFound.html')
        self.response.write(template.render(template_values))


class BookResource(webapp2.RequestHandler):

    def post(self):
        user = users.get_current_user()
        currentResourceID = self.request.get('res')
        template_values = { 'resourceID': currentResourceID,
            'userID': user.nickname()
        }

        template = JINJA_ENVIRONMENT.get_template('BookResource.html')
        self.response.write(template.render(template_values))
        
class DisplayUserDetails(webapp2.RequestHandler):

    def get(self):
        userID = self.request.get("UserID")
        MyReservationList = list(Booking.gql("WHERE UserID = :1" , userID))
        MyResourceList = list(Resource.gql("WHERE OwnerName = :1" , userID))
        MyActiveReservationList = []

        MyReservationLen = len(MyReservationList)
        MyResourceLen = len(MyResourceList)
        
        for passnum in range(len(MyResourceList)-1,0,-1):
            for i in range(passnum):
                if MyResourceList[i].LastReservationTime < MyResourceList[i+1].LastReservationTime:
                    temp = MyResourceList[i]
                    MyResourceList[i] = MyResourceList[i+1]
                    MyResourceList[i+1] = temp

        for passnum in range(len(MyReservationList)-1,0,-1):
            for i in range(passnum):
                if MyReservationList[i].StartTime > MyReservationList[i+1].StartTime:
                    temp = MyReservationList[i]
                    MyReservationList[i] = MyReservationList[i+1]
                    MyReservationList[i+1] = temp

        for i in range(len(MyReservationList)):
            if MyReservationList[i].EndTime > getCurrentTime():
                MyActiveReservationList.append(MyReservationList[i])

        MyActiveReservationLen = len(MyActiveReservationList)

        template_values = { 'MyResourceList': MyResourceList,
                            'MyReservationList': MyActiveReservationList,
                            'MyReservationLen': MyActiveReservationLen,
                            'MyResourceLen': MyResourceLen,
                            'source': "2"
            }

        template = JINJA_ENVIRONMENT.get_template('HomePageHTML.html')
        self.response.write(template.render(template_values))

class DisplayTags(webapp2.RequestHandler):

    def get(self):
        tagName=self.request.get("TagName");
        resourceList = list(Resource.gql(""))
        taggedResourceList = []

        for resource in resourceList:
            for tag in resource.Tags:
                if tagName.upper() == tag.upper():
                    taggedResourceList.append(resource)
                    break

        template_values = {
                'taggedResourceList': taggedResourceList,
                'tag': tagName
        }

        template = JINJA_ENVIRONMENT.get_template('ShowTaggedResources.html')
        self.response.write(template.render(template_values))

def ResourceEditError(self, resource, errorCondition, tagList):
    template_values = {
                'resource': resource,
                'errorCondition': errorCondition,
                'tagList': tagList
    }

    template = JINJA_ENVIRONMENT.get_template('EditResource.html')
    self.response.write(template.render(template_values))

class EditResource(webapp2.RequestHandler):

    def get(self):
        resourceID = self.request.get("resID");
        resource = list(Resource.gql('WHERE ResourceID = :1', resourceID))
        tagList = ""

        for i in range(len(resource[0].Tags)):
            if i == (len(resource[0].Tags)-1):
                tagList = tagList + resource[0].Tags[i]
            else:
                tagList = tagList + resource[0].Tags[i] + ","

        template_values = {
                'resource': resource[0],
                'tagList': tagList
        }

        template = JINJA_ENVIRONMENT.get_template('EditResource.html')
        self.response.write(template.render(template_values))

    def post(self):        
        resourceID = self.request.get("ResID");
        resourceName = self.request.get("name")
        StartTime = self.request.get("from")
        EndTime = self.request.get("to")
        Tags = self.request.get("tags")
        resource = list(Resource.gql('WHERE ResourceID = :1', resourceID))
        tagList = ""

        for i in range(len(resource[0].Tags)):
            if i == (len(resource[0].Tags)-1):
                tagList = tagList + resource[0].Tags[i]
            else:
                tagList = tagList + resource[0].Tags[i] + ","

        if not resourceName:
            ResourceEditError(self, resource[0], "ERROR: The field 'Resource Name' cannot be left blank!", tagList)
        elif not StartTime:
            ResourceEditError(self, resource[0], "ERROR: The field 'Available From' cannot be left blank!", tagList)
        elif not EndTime:
            ResourceEditError(self, resource[0], "ERROR: The field 'Available To' cannot be left blank!", tagList) 
        elif not validate_time(StartTime):
            ResourceEditError(self, resource[0], "ERROR: The field 'Available From' is not in the correct format (HH:MM)!", tagList)
        elif not validate_time(EndTime):
            ResourceEditError(self, resource[0], "ERROR: The field 'Available To' is not in the correct format (HH:MM)!", tagList)
        elif StartTime > EndTime:
            ResourceEditError(self, resource[0], "ERROR: The field 'Available From' cannot be greater than the field 'Available To'!", tagList)
        else:
            resource[0].ResourceName = resourceName
            resource[0].StartTime = StartTime
            resource[0].EndTime = EndTime
            resource[0].Tags = Tags.split(',')
            resource[0].put()

            template_values = {
            }

            template = JINJA_ENVIRONMENT.get_template('ResourceEdited.html')
            self.response.write(template.render(template_values))

def ResourceBookingError(self, resourceID, errorCondition):
    template_values = {
                'resourceID': resourceID,
                'errorCondition': errorCondition
    }

    template = JINJA_ENVIRONMENT.get_template('BookResource.html')
    self.response.write(template.render(template_values))

def getEndTime(StartTime, Duration):

    t1 = list(StartTime.split(":"))
    t2 = list(Duration.split(":"))

    h = int(t1[0]) + int(t2[0])
    m = int(t1[1]) + int(t2[1])

    if m>=60:
        m = m - 60
        h = h + 1

    h_str = str(h)
    m_str = str(m)

    if len(h_str) == 1:
        h_str = "0"+h_str

    if len(m_str) == 1:
        m_str = "0"+m_str

    return h_str + ":" + m_str
    

def validate_dayBoundary(StartTime, Duration):
    EndTime = getEndTime(StartTime, Duration) 
     
    try:
        datetime.datetime.strptime(EndTime, '%H:%M')
        return True
    except ValueError:
        return False

class CheckAvailability(webapp2.RequestHandler):

    def post(self):
        user = users.get_current_user()
        StartTime = self.request.get('from')
        Duration = self.request.get('duration')
        Date = self.request.get('date')
        resourceID = self.request.get('resourceID')
        resource = list(Resource.gql('WHERE ResourceID = :1', resourceID))
        bookingsList = list(Booking.gql('WHERE ResourceID = :1', resourceID))
        userBookingList = list(Booking.gql('WHERE UserID = :1', user.nickname()))

        if not StartTime:
            ResourceBookingError(self, resourceID, "ERROR: The field 'Start Time' cannot be left blank!")
        elif not Duration:
            ResourceBookingError(self, resourceID, "ERROR: The field 'Duration' cannot be left blank!")
        elif not Date:
            ResourceBookingError(self, resourceID, "ERROR: The field 'Reservation Date' cannot be left blank!")
        elif not validate_time(StartTime):
            ResourceBookingError(self, resourceID, "ERROR: The field 'Start Time' is not in the correct format (HH:MM)!")
        elif not validate_time(Duration):
            ResourceBookingError(self, resourceID, "ERROR: The field 'Duration' is not in the correct format (HH:MM)!")
        elif not validate_date(Date):
            ResourceBookingError(self, resourceID, "ERROR: The field 'Reservation Date' is not in the correct format (HH:MM)!")
        elif (Date + ":" + StartTime) < getCurrentTime():
            ResourceBookingError(self, resourceID, "ERROR: The 'StartTime' field cannot be lesser than the current time!")
        elif not validate_dayBoundary(StartTime, Duration):
            ResourceBookingError(self, resourceID, "ERROR: The 'End Time' can't be greater than the Day Boundary(23:59)!")
        elif findIfBusy(resource, userBookingList, Date +":"+StartTime, Date+":"+getEndTime(StartTime, Duration)):
            ResourceBookingError(self, resourceID, "ERROR: You already have another resevation at this time!")
        elif not (StartTime >= resource[0].StartTime and getEndTime(StartTime, Duration) <= resource[0].EndTime):
            ResourceBookingError(self, resourceID, "ERROR: The resource is not available in the requested time slot!")
        elif findIfBusy(resource, bookingsList, Date +":"+StartTime, Date +":"+getEndTime(StartTime, Duration)):
            ResourceBookingError(self, resourceID, "ERROR: The resource is already booked in the requested time slot!")
        else:
            booking = Booking(parent=booking_key(user.nickname()))
            booking.BookingID = str(uuid.uuid1())
            booking.UserID = user.nickname()
            booking.StartTime = Date +":"+StartTime
            booking.EndTime = Date +":"+getEndTime(StartTime, Duration)
            booking.Duration = Duration
            booking.ResourceID = resourceID
            booking.ResourceName = resource[0].ResourceName
            currentTime = (datetime.datetime.now() - datetime.timedelta(hours=5))
            resource[0].LastReservationTime = datetime.datetime.strptime(str(currentTime.year)+":"+str(currentTime.month)+":"+str(currentTime.day)+":"+str(currentTime.hour)+":"+str(currentTime.minute)+":"+str(currentTime.second), "%Y:%m:%d:%H:%M:%S")
            resource[0].Counter = resource[0].Counter + 1 
            resource[0].put()
            booking.put()
            template_values = {
            }

            template = JINJA_ENVIRONMENT.get_template('BookingAdded.html')
            self.response.write(template.render(template_values))

def findIfBusy(resource, bookingsList, StartTime, EndTime):
    for booking in bookingsList:
        if ((booking.StartTime > StartTime and booking.StartTime < EndTime) or (booking.EndTime > StartTime and booking.EndTime < EndTime)):
            return True
        if (booking.StartTime == StartTime or booking.EndTime == EndTime):
            return True
    return False

def getCurrentTime():
    currentTime = datetime.datetime.now() - datetime.timedelta(hours=5)
    h = str(currentTime.hour)
    m = str(currentTime.minute)
    d = str(currentTime.day)
    mo = str(currentTime.month)
    y = str(currentTime.year)

    if len(h) == 1:
        h = "0"+h

    if len(m) == 1:
        m = "0"+m

    if len(d) == 1:
        d = "0"+d

    if len(m) == 1:
        mo = "0"+mo

    return y + ":" + mo + ":" + d + ":" + h + ":" + m 


    return h + ":" + m 

class ShowResource(webapp2.RequestHandler):

    def get(self):
        resourceID = self.request.get('ResID')
        resource = list(Resource.gql('WHERE ResourceID = :1', resourceID))
        resourceReservationList = list(Booking.gql("WHERE ResourceID = :1" , resourceID))
        MyActiveReservationList = []

        if self.request.get("TableNumber") == "3":
            editable = "Yes"
        else:
            editable = "No"

        for passnum in range(len(resourceReservationList)-1,0,-1):
            for i in range(passnum):
                if resourceReservationList[i].StartTime > resourceReservationList[i+1].StartTime:
                    temp = resourceReservationList[i]
                    resourceReservationList[i] = resourceReservationList[i+1]
                    resourceReservationList[i+1] = temp

        for i in range(len(resourceReservationList)):
            if resourceReservationList[i].EndTime > getCurrentTime():
                MyActiveReservationList.append(resourceReservationList[i])    

        template_values = { 'resource': resource[0],
            'resourceReservationList': MyActiveReservationList,
            'resourceReservationLen': len(MyActiveReservationList),
            'editable': editable 
        }

        template = JINJA_ENVIRONMENT.get_template('ShowResource.html')
        self.response.write(template.render(template_values))

class CancelBooking(webapp2.RequestHandler):

    def get(self):
        bookingID = self.request.get("BookingID")
        BookingList = list(Booking.gql("WHERE BookingID = :1" , bookingID))
        resource = list(Resource.gql('WHERE ResourceID = :1', BookingList[0].ResourceID))

        
        resource[0].Counter = resource[0].Counter - 1
        
        resource[0].put()

        for booking in BookingList:
            booking.key.delete()

        template_values = {  
        }

        template = JINJA_ENVIRONMENT.get_template('ReservationDeleted.html')
        self.response.write(template.render(template_values))

def validate_time(dateString):

    try:
        datetime.datetime.strptime(dateString, '%H:%M')
        return True
    except ValueError:
        return False

def validate_date(dateString):

    try:
        datetime.datetime.strptime(dateString, '%Y:%m:%d')
        return True
    except ValueError:
        return False    

def ResourceCreationError(self, user, errorCondition):
    template_values = {
                'user': user,
                'errorCondition': errorCondition
    }

    template = JINJA_ENVIRONMENT.get_template('CreateResourceHTML.html')
    self.response.write(template.render(template_values))

def FindAvailabilitiesError(self, errorCondition):
    template_values = {
                'errorCondition': errorCondition
    }

    template = JINJA_ENVIRONMENT.get_template('ResourceCreationError.html')
    self.response.write(template.render(template_values))

class FindAvailableResources(webapp2.RequestHandler):

    def post(self):
        Duration = self.request.get("Duration")
        Date = self.request.get("Date")
        StartTime = self.request.get("From")

        if not StartTime:
            FindAvailabilitiesError(self, "ERROR: The field 'Start Time' cannot be left blank!")
        elif not Duration:
            FindAvailabilitiesError(self, "ERROR: The field 'Duration' cannot be left blank!")
        elif not Date:
            FindAvailabilitiesError(self, "ERROR: The field 'Date' cannot be left blank!")
        elif not validate_time(StartTime):
            FindAvailabilitiesError(self, "ERROR: The field 'Start Time' is not in the correct format (HH:MM)!")
        elif not validate_time(Duration):
            FindAvailabilitiesError(self, "ERROR: The field 'Duration' is not in the correct format (HH:MM)!")
        elif not validate_date(Date):
            FindAvailabilitiesError(self, "ERROR: The field 'Date' is not in the correct format (YYYY:MM:DD)!")
        elif not validate_dayBoundary(StartTime, Duration):
            FindAvailabilitiesError(self, "ERROR: The 'End Time' can't be greater than the Day Boundary(23:59)!")
        else:
            EndTime = getEndTime(StartTime,Duration)
            FormattedEndTime = Date + ":" + getEndTime(StartTime,Duration)
            FormattedStartTime = Date + ":" + StartTime
            
            allResourceList = list(Resource.gql(""))
            availableResourceList = []

            for resource in allResourceList:
                bookingList = list(Booking.gql("WHERE ResourceID= :1", resource.ResourceID))
                if ((StartTime >= resource.StartTime and getEndTime(StartTime, Duration) <= resource.EndTime) and (not findIfBusy(resource, bookingList, FormattedStartTime, FormattedEndTime))):
                    availableResourceList.append(resource)

            template_values = {
                'availableResourceList': availableResourceList,
                'availableResourceLen': len(availableResourceList)
            }

            template = JINJA_ENVIRONMENT.get_template('ShowAvailableResources.html')
            self.response.write(template.render(template_values))

class CreateResource(webapp2.RequestHandler):

    def get(self):

        user = users.get_current_user()

        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            self.redirect(users.create_login_url(self.request.uri))

        template_values = {
            'user': user.nickname()
        }

        template = JINJA_ENVIRONMENT.get_template('CreateResourceHTML.html')
        self.response.write(template.render(template_values))

    def post(self):
        obj = datetime.datetime.now()

        resourceObj = Resource(parent=resource_key(str(obj.day)+str(obj.month)+str(obj.year)))
        resourceObj.ResourceID = str(uuid.uuid1())
        resourceObj.ResourceName = self.request.get('name')
        resourceObj.StartTime = self.request.get('from')
        resourceObj.EndTime = self.request.get('to')
        resourceObj.OwnerName = self.request.get('owner')
        resourceObj.Tags = self.request.get('tags').split(',')
        resourceObj.Counter = 0
        resourceObj.LastReservationTime = datetime.datetime.strptime("1970:01:01:00:00:00", "%Y:%m:%d:%H:%M:%S")

        if not resourceObj.ResourceName:
            ResourceCreationError(self, self.request.get('owner'), "ERROR: The field 'Resource Name' cannot be left blank!")
        elif not resourceObj.StartTime:
            ResourceCreationError(self, self.request.get('owner'), "ERROR: The field 'From' cannot be left blank!")
        elif not resourceObj.EndTime:
            ResourceCreationError(self, self.request.get('owner'), "ERROR: The field 'To' cannot be left blank!")
        elif not validate_time(resourceObj.StartTime):
            ResourceCreationError(self, self.request.get('owner'), "ERROR: The field 'From' is not in the correct format (HH:MM)!")
        elif not validate_time(resourceObj.EndTime):
            ResourceCreationError(self, self.request.get('owner'), "ERROR: The field 'To' is not in the correct format (HH:MM)!")
        elif resourceObj.StartTime > resourceObj.EndTime:
            ResourceCreationError(self, self.request.get('owner'), "ERROR: The 'To' field can't be lesser than the 'From' field!")
        else:
            resourceObj.put()
            template_values = {
            }

            template = JINJA_ENVIRONMENT.get_template('ResourceCreated.html')
            self.response.write(template.render(template_values))

class ShowRSSDump(webapp2.RequestHandler):
    def get(self):
        ResourceID = self.request.get("ResID")
        resource = list(Resource.gql('WHERE ResourceID = :1', ResourceID))
        BookingList = list(Booking.gql("WHERE ResourceID = :1" , ResourceID))

        template_values = { 'BookingList': BookingList,
                            'ResourceName': resource[0].ResourceName
        }

        template = JINJA_ENVIRONMENT.get_template('ShowRSSDump.html')
        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/book', BookResource),
    ('/create', CreateResource),
    ('/resource', ShowResource),
    ('/availability', CheckAvailability),
    ('/tag', DisplayTags),
    ('/editResource', EditResource),
    ('/showUser', DisplayUserDetails),
    ('/cancel', CancelBooking),
    ('/dump', ShowRSSDump),
    ('/search', SearchResource),
    ('/findAvailabilities', FindAvailableResources)
], debug=True)